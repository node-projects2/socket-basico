var socket = io();

// Los on son para quedarse escuchando
socket.on('connect', function () {
    console.log('conectado al servidor por sockets');
});
socket.on('disconnect', function () {
    console.log('Perdimos conexión con el servidor');
});

socket.on('enviarMensaje', function (mensaje) {
    console.log('Mensaje del servidor', mensaje);
});

// Emit para enviar información
socket.emit('enviarMensaje', {
    usuario: 'David',
    mensaje: 'Hola mundo'
}, function (resp) {
    console.log('Se disparó el callback de enviarmensaje en cliente', resp);
});