const { io } = require('../server')


io.on('connection', (client) => {
    console.log('Usuario conectado');

    client.emit('enviarMensaje', {
        usuario: 'Administrador',
        mensaje: 'Bienvenido a esta aplicación'
    });

    client.on('disconnect', () => {
        console.log('Usuario desconectado');
    });

    // Escuchar información del cliente
    client.on('enviarMensaje', (mensaje, callback) => {
        console.log(mensaje);

        // Esto para enviar sólo al cliente que ha hecho la llamada
        // client.emit('enviarMensaje', mensaje);

        client.broadcast.emit('enviarMensaje', mensaje);

        // if (mensaje.usuario) {
        //     callback({
        //         respuesta: 'TODO SALIÓ BIEN!'
        //     });
        // } else {
        //     callback({
        //         respuesta: 'TODO SALIÓ MAL!!!!!!'
        //     });

        // }

    });
});